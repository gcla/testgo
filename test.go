package main

import (
	"fmt"
)

type ICallMe interface {
	CallMe()
}

type Test struct{}

type Test2 struct {
	Test
}

func (t Test) CallMe() {
	fmt.Println("Test callme")
}

func (t Test2) CallMe() {
	fmt.Println("Test2 callme")
}

func main() {
	fmt.Println("Hello, playground")

	var t Test
	t.CallMe()
	var t2 Test2
	t2.CallMe()

	var ti ICallMe
	ti = t
	ti.CallMe()
	ti = t2
	ti.CallMe()
}
